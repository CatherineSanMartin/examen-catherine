<%-- 
    Document   : historial
    Created on : 11-05-2020, 16:52:52
    Author     : cathe <cathe>
--%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="root.persistence.entities.Diccionario"%>
<%@page import="java.util.List"%>
<%@page import="root.dao.DiccionarioJpaController"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body style="background-image: url(imagen/libro.jpg)">
        <%
            DiccionarioJpaController dao = new DiccionarioJpaController();
            int pCant = dao.getDiccionarioCount();
        %>
        <div class="container">
            <h5 class="centrar">"Palabras buscadas y Almacenadas" <%=pCant%> </h5>
            <div class="centrar">
                <a class="btn btn-primary btn-sm mb-3" align="center" href="index.jsp" target="_top">Volver</a>
            </div>
            <table class="table table-bordered">
                <thead class="thead-dark">
                <tr>
                    <th class="centrar">Fecha Busqueda</th>
                    <th class="centrar">Palabra</th>
                    <th class="centrar">Definicion</th>
                </tr>
                </thead>
                <%
                    List<Diccionario> getPalabras = dao.findDiccionarioEntities();
                    int indice = pCant - 1;
                    if (indice < 0) {
                        indice = 0;
                    }
                    if (pCant > 0) {
                        for (int i = 0; i <= indice; i++) {
                           

                            out.write("<tr>");
                            
                            out.write("<td class='text-center'>" + getPalabras.get(i).getPalabra() + "</td>");
                            out.write("<td class='text-center'>" + getPalabras.get(i).getDefinicion() + "</td>");
                            out.write("</tr>");
                        }
                    } else {
                        out.write("<tr>");
                        out.write("<td class='text-center'> No existen registros </td> ");
                        out.write("</tr>");
                    }

                %>
            </table>
        </div>
    </body>
</html>
