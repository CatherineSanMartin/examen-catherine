/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.services;

/**
 *
 * @author cathe <cathe>
 */
public class Validaciones {
    public static String eliminarAcentos(String str) {

        final String ACENTOS = "ÁáÉéÍíÓóÚúÑñÜü";
        final String CAMBIO = "AaEeIiOoUuNnUu";

        if (str == null) {
            return null;
        }
        char[] array = str.toCharArray();
        for (int indice = 0; indice < array.length; indice++) {
            int pos = ACENTOS.indexOf(array[indice]);
            if (pos > -1) {
                array[indice] = CAMBIO.charAt(pos);
            }
        }
        return new String(array);
    }
}
