
package root.services;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONObject;
import root.persistence.entities.Diccionario;

@Path("/diccionario")
public class diccionarioRest {
    
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("examen_PU");
        EntityManager em;
        
  @GET
    @Path("/{pBuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscarPalabra(@PathParam("pBuscar") String pBuscar) throws IOException, ParseException {

        pBuscar = Validaciones.eliminarAcentos(pBuscar);
        pBuscar = pBuscar.toLowerCase();

        OxfordRest buscar = new OxfordRest();
        JSONObject jsonOxford = buscar.palabraOxford(pBuscar);

        String def = jsonOxford.get("0").toString();
        char cZero = def.charAt(0);
        if (cZero == '*') {
            def = "Palabra: '" + pBuscar + "' no Existe";
        }

        return Response.ok(200).entity(def).build();
    }
}

