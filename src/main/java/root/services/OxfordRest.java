/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.services;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import java.io.IOException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author cathe <cathe>
 */
public class OxfordRest {
   
    @GET 
    @Path("/{pBuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    public JSONObject palabraOxford(@PathParam("pBuscar") String pBuscar) throws IOException{

   
        String language = "es/";

        String baseURL = "https://od-api.oxforddictionaries.com/api/v2/entries/" + language;
        String parametrosURL = "?fields=definitions&strictMatch=false";

     
         OkHttpClient clienteWEB = new OkHttpClient();
        Request request = new Request.Builder()
                .url(baseURL + pBuscar + parametrosURL)
                .get()
                .addHeader("Content-Type","text/json;Charset=UTF-8")
                .addHeader("app_id", "73f18c04")
                .addHeader("app_key", "d6fa1e8e449cf3ed0b55adfae9931503")
                .build();
        com.squareup.okhttp.Response respuesta = clienteWEB.newCall(request).execute();

         try {
            JSONObject jsonFull = new JSONObject(respuesta.body().string());
            JSONArray arrayResults = jsonFull.getJSONArray("results");

            JSONObject json1 = arrayResults.getJSONObject(0);
            JSONArray arrayLexEntries = json1.getJSONArray("lexicalEntries");

            json1 = arrayLexEntries.getJSONObject(0);
            JSONArray arrayEntries = json1.getJSONArray("entries");

            json1 = arrayEntries.getJSONObject(0);
            JSONArray arraySenses = json1.getJSONArray("senses");

            JSONObject jsonSalida = new JSONObject();

            String auxMsg = "";

            for (int i = 0; i < arraySenses.length(); i++) {
                auxMsg = arraySenses.getJSONObject(i).get("definitions").toString().substring(2);
                auxMsg = auxMsg.substring(0, auxMsg.length() - 2);
                jsonSalida.put("" + i, auxMsg);
            }
            return jsonSalida; 
        } catch(Exception e) {
            JSONObject jsonSalida = new JSONObject();
            jsonSalida.put("0","*** ERROR ***: Palabra No Existe, intente con una nueva.");
            return jsonSalida;
        }
    }
    
}
