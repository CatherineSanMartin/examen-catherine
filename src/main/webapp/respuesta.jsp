<%-- 
    Document   : respuesta
    Created on : 11-05-2020, 16:53:18
    Author     : cathe <cathe>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Respuesta</title>
    </head>
    <body style="background-image: url(imagen/libro.jpg)">
       <% 
            String mensaje = (String) request.getAttribute("mensajeSalida");
        %>
        <table class="table">
            <div class="centrar">
                <h5 class="centrar">Definición : <%=mensaje%></h5>
            </div>
            <tr>
                <td class="text-center"><a class="btn btn-primary" align="center" href="index.jsp" target="_top">Atras</a></td>
                
            </tr>
        </table>
    </body>
</html>
