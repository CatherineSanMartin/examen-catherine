<%-- 
    Document   : buscar
    Created on : 11-05-2020, 16:52:39
    Author     : cathe <cathe>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body style="background-image: url(imagen/libro.jpg)">
        <div class="container">
            <form action="controller" method="POST">
                <table class="table">
                    <tr>
                        <td>
                            <h6 class="text-center">Ingrese Palabra:</h6>
            </td>
        </tr>
        <tr >
            <td class="text-center col-md-4">
                <input class="form-control" type="text" name="iPalabra" value="" size="20" maxlength="20" autocomplete="off" required>
            </td>
        </tr>
        <tr>
            <td class="text-center">
                <input type="text" hidden="true" name="accion" value="search" />
                <input type="submit"  value="Buscar" />
                <input type="reset"  value="Limpiar" />
            </td>
        </tr>
                </table>     
            </form>
        </div>

    </body>
</html>
